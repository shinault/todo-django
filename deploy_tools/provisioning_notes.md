Provisioning a new site
=======================

## Required packages:

* nginx
* Python 3.6
* virtualenv + pip
* Git

To install on Ubuntu 16.04, first add PPA.

```bash
sudo add-apt-repository ppa:deadsnakes/ppa
```

Then for 16.04 or 18.04, run

```bash
sudo apt update
sudo apt install nginx git python36 python3.6-venv
```

## Nginx Virtual Host config

* See `nginx.template.conf`
* Replace `DOMAIN` with e.g. staging.mydomain.com


## Systemd service

* See `gunicorn-systemd.template.service`
* Replace `DOMAIN` with e.g. staging.mydomain.com


## Folder structure

Assuming we have a user account at `/home/username`

```bash
└── sites
    ├── DOMAIN1
    │    ├── .env
    │    ├── db.sqlite3
    │    ├── manage.py etc
    │    ├── static
    │    └── virtualenv
    └── DOMAIN2
         ├── .env
         ├── db.sqlite3
         ├── etc
```
